# **libvdwxc** - van der Waals density functional library

[**libvdwxc**](https://libvdwxc.materialsmodeling.org/) [1] is a library of density functionals, which can be linked by
density functional theory (DFT) codes, to enable the calculation of exchange
and correlation energies for van der Waals density functionals (vdW-DF) [3,4].
**libvdwxc** evaluates correlation energy and potential using a convolution in
Fourier space using the method by Román-Pérez and Soler [2].

```math
  E[\rho] = \frac{1}{2} \iint \rho(\mathbf{r}) K(\mathbf{r}, \mathbf{r}')  \rho(\mathbf{r}') d\mathbf{r} d\mathbf{r}'
  = \frac{1}{2} \sum_{ab} \int \theta_a^*(\mathbf{k}) K_{ab}(\mathbf{k}) \theta_b(\mathbf{k}) d\mathbf{k},
```

where $`K(\mathbf{r}, \mathbf{r}')`$ is the kernel of the vdW functional, and
$`\theta(\mathbf k)`$ are auxiliary functions derived from the density. The
indices $`a`$ and $`b`$ refer to an expansion of each quantity in a basis of
splines.  The effect is to reduce the 6D spatial integral over $`\mathbf{r}`$ and
$`\mathbf{r}'`$ to a sum of 3D integrals.

Since **libvdwxc** uses Fourier transforms, it works on electron densities
that are defined on uniform grids within parallel epiped-shaped boxes.
**libvdwxc** works in parallel with MPI and uses FFTW-MPI or PFFT to
parallelize the three-dimensional Fourier transforms.

## Documentation

A comprehensive documentation of **libvdwxc** can be found
[on its homepage](https://libvdwxc.materialsmodeling.org/).
**libvdwxc** and its development are hosted on
[gitlab](https://gitlab.com/libvdwxc/libvdwxc). Bugs and feature
requests are ideally submitted via the [gitlab issue
tracker](https://gitlab.com/libvdwxc/libvdwxc/issues).

If you use **libvdwxc** in your research please include
the following citation in publications or presentations:

* A. H. Larsen, M. Kuisma, J. Löfgren, Y. Pouillon, P. Erhart, and P. Hyldgaard,
  *libvdwxc: a library for exchange–correlation functionals in the vdW-DF family*,
  Modelling Simul. Mater. Sci. Eng. **25**, 065004 (2017),
  [doi: 10.1088/1361-651X/aa7320](https://dx.doi.org/10.1088/1361-651X/aa7320)

## Supported functionals
Presently the library calculates the non-local correlation energy and
potential of the functionals

 * vdW-DF1 [3]
 * vdW-DF2 [4]

These can be combined with different semilocal functionals, generally
provided by [**libxc**](https://libxc.gitlab.io/),
to form full functionals consisting of
a semilocal exchange functional (X) plus LDA correlation (C) and
the non-local functional corresponding to vdW-DF1 or vdW-DF2:

 * vdW-DF1 (revPBE X + LDA C + DF1 non-local)
 * vdW-DF2 (RPW86 X + LDA C + DF2 non-local)
 * vdW-DF-cx [5] (Langreth–Vosko/RPW86 X + LDA C + DF1 non-local)
 * vdW-optPBE (modified PBE X + LDA C + DF1 non-local)
 * vdW-optB88 (modified B88 X + LDA C + DF1 non-local)
 * vdW-C09 (C09x X + LDA C + DF1 non-local)
 * vdW-BEEF (semilocal BEE2 kernel + DF2 non-local)
 * vdW-mBEEF (semilocal BEE3 kernel + (0.89 x) DF2 non-local)


## References

[1] A. H. Larsen, M. Kuisma, J. Löfgren, Y. Pouillon, P. Erhart, and P. Hyldgaard,
[Modelling and Simulation in Materials Science and Engineering **25**, 065004 (2017)](https://dx.doi.org/10.1088/1361-651X/aa7320).

[2] G. Román-Pérez and J. M. Soler,
[Physical Review Letters **103**, 096102 (2009)](https://dx.doi.org/10.1103/PhysRevLett.103.096102).

[3] M. Dion, H. Rydberg, E. Schröder, D. C. Langreth, and B. I. Lundqvist,
[Physical Review Letters **92**, 246401 (2004)](https://doi.org/10.1103/PhysRevLett.92.246401).

[4] K. Lee, E. D. Murray, L. Kong, B. I. Lundqvist, and D. C. Langreth,
[Physical Review B **82**, 081101 (2010)](https://doi.org/10.1103/PhysRevB.82.081101).

[5] K. Berland and P. Hyldgaard,
[Physical Review B **89**, 035412 (2014)](https://doi.org/10.1103/PhysRevB.89.035412).
