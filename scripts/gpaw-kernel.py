from gpaw import GPAW
from gpaw.xc.vdw import FFTVDWFunctional
import numpy as np

print "Extracting kernel file from GPAW..."

def write_array(f, name, array):
    print >>f, name
    for dim in array.shape:
        print >>f, dim,
    c = 0
    for data in array.ravel():
        if c % 4 == 0:
            print >>f
        print >>f, data,
        c += 1
    print >>f

f = open('gpaw.kernel','w')
vdw = FFTVDWFunctional()
print "Constructing cubic splines"
vdw.construct_cubic_splines()
print "Constructing fourier transformed kernels"
vdw.construct_fourier_transformed_kernels()
write_array(f,'q0 mesh', vdw.q_a)
write_array(f,'Cubic spline coefficients', vdw.C_aip)

M = vdw.Nr
rcut = vdw.rcut 
k_j = np.arange(M // 2) * (2 * np.pi / rcut)
NA = len(vdw.q_a)
kernel = np.zeros((NA, NA, M//2, 4))
for a in range(NA):
    for b in range(NA):
        kernel[a,b] = vdw.phi_aajp[a,b]
write_array(f,'dk', np.array([ 2*np.pi / rcut]))
write_array(f,'Fourier transformed kernels', kernel)
f.close()
print "Wrote gpaw.kernel."
