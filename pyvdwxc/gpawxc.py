import numpy as np
from gpaw.xc.libxc import LibXC
from gpaw.xc.functional import XCFunctional
from gpaw.xc.gga import GGA
from gpaw.xc.vdw import VDWFunctional
from gpaw.utilities.grid_redistribute import redistribute

from pyvdwxc import VDWXC
from pyvdwxc.old_ours import VDW as VDWOld, get_q0_on_grid

def check_grid_descriptor(gd):
    assert gd.parsize_c[1] == 1 and gd.parsize_c[2] == 1
    nxpts_p = gd.n_cp[0][1:] - gd.n_cp[0][:-1]
    nxpts0 = nxpts_p[0]
    for nxpts in nxpts_p[1:-1]:
        assert nxpts == nxpts0
    assert nxpts_p[-1] <= nxpts0
    #print nxpts_p

class GPAWVDWXCFunctional(GGA):
    def __init__(self):
        GGA.__init__(self, LibXC('GGA_X_PBE_R+LDA_C_PW'))
        self._lib = None #VDWXC()
        self._initialized = False

    def get_setup_name(self):
        return 'revPBE'

    def lib_init(self, N_c, cell_cv):
        # XXX parallel
        self._lib = VDWXC(N_c, cell_cv)
        #self._lib.set_unit_cell(N_c, cell_cv)
        #self._lib.set_topology(np.arange(np.prod(N_c)))
        self._initialized = True

    def initialize(self, density, hamiltonian, wfs, occupations):
        GGA.initialize(self, density, hamiltonian, wfs, occupations)
        gd = density.finegd
        nx, ny, nz = gd.parsize_c

        self.gd1 = gd
        self.gd2 = gd.new_descriptor(parsize=(nx, ny * nz, 1))
        self.gd3 = self.gd2.new_descriptor(parsize=(nx * ny * nz, 1, 1))
        check_grid_descriptor(self.gd3)
        N_c = gd.get_size_of_global_array()
        self.lib_init(N_c, gd.cell_cv)

    # This one we write down just to "echo" the original interface
    def calculate(self, gd, n_sg, v_sg, e_g=None):
        #check_grid_descriptor(gd)
        if e_g is None:
            e_g = gd.zeros()
        return GGA.calculate(self, gd, n_sg, v_sg, e_g=e_g)
    
    def calculate_gga(self, e_g, n_sg, v_sg, sigma_xg, dedsigma_xg):
        assert self._initialized
        self.n_sg = n_sg
        self.sigma_xg = sigma_xg
        #print 'nmin', n_sg.min()
        #print 'hmmmm', sum(n_sg[0].ravel() < 0), sum(n_sg[0].ravel() > 0)
        n_sg[:] = np.abs(n_sg)
        sigma_xg[:] = np.abs(sigma_xg)
        assert len(n_sg) == 1
        assert len(sigma_xg) == 1
        GGA.calculate_gga(self, e_g, n_sg, v_sg, sigma_xg, dedsigma_xg)

        n1_g = n_sg[0]
        sigma1_g = sigma_xg[0]
        

        n2_g = redistribute(self.gd1, self.gd2, n1_g, 1, 2)
        n3_g = redistribute(self.gd2, self.gd3, n2_g, 0, 1)

        sigma2_g = redistribute(self.gd1, self.gd2, sigma1_g, 1, 2)
        sigma3_g = redistribute(self.gd2, self.gd3, sigma2_g, 0, 1)


        #sigma3_g = self.gd3.zeros(dtype=float)
        
        Ecnl, v3_g, dedsigma3_g = self._lib.calculate(n3_g, sigma3_g)
        
        v2_g = redistribute(self.gd2, self.gd3, v3_g, 0, 1, operation='back')
        v1_g = redistribute(self.gd1, self.gd2, v2_g, 1, 2, operation='back')

        dedsigma2_g = redistribute(self.gd2, self.gd3, dedsigma3_g, 0, 1,
                                   operation='back')
        dedsigma1_g = redistribute(self.gd1, self.gd2, dedsigma2_g, 1, 2,
                                   operation='back')


        self.Ecnl = Ecnl
        self.vnl_g = v1_g
        #q0_g = np.zeros_like(n_sg[0])
        #dq0dg_g = np.zeros_like(n_sg[0])
        #dq0_dsigma_g = np.zeros_like(n_sg[0])

        #n_sg[:] = 1e-1
        #sigma_xg[:] = 0.0
        
        #_q0, _dq0_drho, _dq0_dgradrho = get_q0_on_grid(n_sg[0], sigma_xg[0])

        #self._lib.calculate_q0(-0.8491, 5.0, n_sg[0], sigma_xg[0],
        #                       q0_g, dq0dg_g, dq0_dsigma_g)

        #print 'q0max %f vs %f' % (q0_g.max(), _q0.max())
        #print 'dq0dg %f vs %f' % (dq0dg_g.max(), _dq0_drho.max())
        #print 'dq0_dsigma_g %f vs %f' % (dq0_dsigma_g.max(), _dq0_dgradrho.max())
        if 0:
            import pylab as pl
            sh = n_sg[0].shape
            sl1 = _q0[sh[0] // 2 + 2, sh[1] // 2 + 2, :]
            sl2 = q0_g[sh[0] // 2 + 2, sh[1] // 2 + 2, :]

            for qslice in q0_g:
                pl.matshow(qslice)
            #pl.plot(sl1)
            #pl.plot(sl2)
            pl.show()

        self.Ecnl = Ecnl
        print "E non local", Ecnl
        v_sg[0, :] += v1_g
        dedsigma_xg[0, :] += dedsigma1_g
        return Ecnl # XXX is not actually supposed to return anything
        # Energy should be added to e_g

def main():
    from ase import Atoms
    from gpaw import GPAW

    gpaw_xc = VDWFunctional('vdW-DF', soft_correction=0)
    print 'gpaw xcname', gpaw_xc.get_setup_name()
    print 'orb dep', gpaw_xc.orbital_dependent
    print 'gpaw type', gpaw_xc.type
    
    system = Atoms('He')
    system.center(vacuum=2.0)
    system.pbc = 1
    xc_ours = GPAWVDWXCFunctional()

    if 0:
        calc = GPAW(xc=xc_ours, txt='gpaw.libvdwxc.txt')
    else:
        calc = GPAW(xc=gpaw_xc, txt='gpaw.origvdw.txt')
    def stopcalc():
        calc.scf.converged = True
    calc.attach(stopcalc, 6)
    
    system.set_calculator(calc)
    system.get_potential_energy()

    xc_ours.initialize(calc.density, calc.hamiltonian, calc.wfs,
                       calc.occupations)
    gd = calc.density.finegd

    def xccalc(xc):
        n_sg = calc.density.nt_sg.copy()
        v_sg = np.zeros_like(n_sg)
        e_g = np.zeros_like(n_sg[0])
        #n_sg[:] = 1.0
        Ecnl = xc.calculate(gd, n_sg, v_sg=v_sg, e_g=e_g)
        return Ecnl
    vdw_old_ours = VDWOld()
    vdw_old_ours.initialize(calc.density, calc.hamiltonian, calc.wfs,
                            calc.occupations)
    
    e1 = xccalc(xc_ours)
    e2 = xccalc(gpaw_xc)
    e3 = xccalc(vdw_old_ours)

    e1 = xc_ours.Ecnl
    #gpaw_xc.get_non_local_energy()
    e2 = gpaw_xc.Ecnl
    e3 = vdw_old_ours.Ecnl

    #print 'v err', np.abs(xc_ours.vnl_g - vdw_old_ours.vnl_g).max()

    

    import pylab as pl
    #pl.plot(np.abs((xc_ours.vnl_g - vdw_old_ours.vnl_g).ravel()))
    #pl.plot(np.abs((xc_ours.vnl_g - gpaw_xc.vnl_g).ravel()))

    #for V in [xc_ours.vnl_g, vdw_old_ours.vnl_g, gpaw_xc.v0_g]:
    #    pl.plot(V[10, 10, :])

    #pl.plot(xc_ours.vnl_g[10, :, :].ravel())
    #pl.plot(vdw_old_ours.vnl_g[10, :, :].ravel())
    pl.show()

    print 'energies %s vs %s vs %s' % (repr(e1), repr(e2), repr(e3))
    
if __name__ == '__main__':
    main()
