import numpy as np
from vdw_ours import get_q0_on_grid
lines = open('../q0test.output').readlines()
N = len(lines)
rho_g = np.zeros((N,))
sigma_g = np.zeros((N,))
for i in range(N):
    data = lines[i].split()
    rho_g[i] = float(data[0])
    sigma_g[i] = float(data[1])
a,b,c = get_q0_on_grid(rho_g, sigma_g)

s_a = 0.0
s_b = 0.0
s_c = 0.0
for i in range(1,N):
    data = lines[i].split()
    liba = float(data[2])
    libb = float(data[3])
    libc = float(data[4])
    print "In", float(data[0]), float(data[1])
    print "Out1", liba, libb, libc
    print "Out2", a[i],b[i],c[i]
    print "Diff", liba-a[i], libb-b[i], libc-c[i]
    assert np.abs((liba-a[i])) < 1e-7
    assert np.abs((libb-b[i])) < 1e-7
    assert np.abs((libc-c[i])) < 1e-7
    s_a += (liba-a[i])**2
    s_b += (libb-b[i])**2
    s_c += (libc-c[i])**2
print "errors", (s_a/N)**0.5, (s_b/N)**0.5, (s_c/N)**0.5
