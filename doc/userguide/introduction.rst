.. _introduction:
.. index:: Introduction

Introduction
************************************

Density functional (DF) theory is widely used in materials and nano
science because it offers an efficient approach for a computational
characterization of structure and response. In its first-principle
formulations it has no adjustable parameters and can thus work also in
a a predictive mode, in the absence of prior measurements. In
traditional semilocal DFT the descriptions were limited to materials
having a dense electron distributions but failed, for example, to
account for binding between molecules.

The Chalmers-Rutgers van der Waals density functional (vdW-DF) method
provides an approach for crafting nonempirical DF that extend the
reach of first-principle DFT also to systems with a sparse electron
distribution, for example, as is found between two molecules. The
existing vdW-DF versions systematically include also truly nonlocal
correlation and can thus capture van der Waals forces at the same
footing as DFT describes other types of interactions. The recent
vdW-DF versions, such as the consistent-exchange vdW-DF-cx, yield a
general-purpose materials description, giving accurate predictions
also when there is a need to simultaneously describe binding in
systems with a genaral electron disctribution.

The Roman-Soler algorithm for evaluating the nonlocal correlation
energy functional of vdW-DF means a significant speed up because it
allow us to relay on a FFT evaluation. At the same time it is clear
that it makes it difficulat to reach very large systems size unless
special attention is given to the implementation. Here we prsent a
library that rests upon a highly scaleable FFT evaluation and hence
allow us to ensure that the vdW-DF can be evaluated at very large
systems sizes.
