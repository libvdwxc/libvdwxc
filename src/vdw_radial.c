#include <assert.h>
#include <math.h>
#include <fftw3.h>

#include "vdw_radial.h"
#include "vdwxc.h"
#include "vdw_kernel.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "vdw_core.h"

#if defined VDW_TIMING
#include "vdw_timing.h"
#endif

#define true 1
#define false 0

static const int SPLINE_ALPHAS = 0;

void vdwxc_write_data(int N, double dr, double* data, char* file)
{
    FILE *fp;
    fp = fopen(file, "w");
    int g;
    for (g=0; g<N; g++) {
       	fprintf(fp, "%.10f %.10f\n", g*dr, data[g]);
    }
    fclose(fp);
}

void vdwxc_fourier_bessel_transform(int Ninput, int Noutput,
                                    int lda_input, int lda_output,
                                    double dr,
                                    int premul_input,
                                    double* input, double* output,
                                    int direction)
{
  assert (direction == VDW_FBT_FORWARD || direction == VDW_FBT_BACKWARD);

  int k;
  int g;

  // For now, just a reference implementation without FFTW.
  if (direction == VDW_FBT_FORWARD) {
     vdwxc_write_data(Ninput, dr, input, "fbtin.txt");
     for (k=0; k<Noutput; k++) {
         output[k*lda_output] = 0.0;
         for (g=0; g<Ninput; g++) {
             if (k == 0) {
                 // What to do to avoid division by zero?
                 //if (premul_input) {
                 //   output[k*lda_output] += 2.0*VDW_PI*g*1.0/Noutput*input[g*lda_input];
                 //} else {
                 output[k*lda_output] += 2.0*VDW_PI*g*g*1.0/Noutput*input[g*lda_input]*dr*dr;
                 //}
             } else {
                 //if (premul_input) {
                 //    output[k*lda_output] += 2*sin(VDW_PI*g*k*1.0/(Noutput))/k*input[g*lda_input];
                 //} else {
                 output[k*lda_output] += 2*g*sin(VDW_PI*g*k*1.0/(Noutput))/k*input[g*lda_input]*dr;
                 //}
             }
         }
         output[k*lda_output] *= 4*VDW_PI;
     }
     vdwxc_write_data(Noutput, dr, output, "fbtout.txt");
  }
  if (direction == VDW_FBT_BACKWARD)
  {
     vdwxc_write_data(Noutput, dr, output, "ifbtin.txt");
     double dk = 2*VDW_PI/(Noutput*dr);
     for (g=0; g<Ninput; g++) {
         input[g*lda_input] = 0.0;
         for (k=0; k<Noutput; k++) {
             if (g == 0) {
                 // What to do to avoid division by zero?
                 input[g*lda_input] += (4*VDW_PI)*VDW_PI*k*k*dk*1.0/Noutput*output[k*lda_output];
                 //double c = k*VDW_PI*k*1.0/Noutput*output[k];
                 //printf("contribution %.10f \n", c);
             } else {
                 input[g*lda_input] += (4*VDW_PI)*k*dk*sin(VDW_PI*g*k*1.0/(Noutput))/g*output[k*lda_output];
             }

         }
         input[g*lda_input] /= (2*VDW_PI)*(2*VDW_PI)*(2*VDW_PI); //((2*VDW_PI)*(2*VDW_PI)*(2*VDW_PI));
     }
     vdwxc_write_data(Ninput, dr, input, "ifbtout.txt");
  }
}


// Debug function, to test the spherical Bessel transform
// OBSOLETE!
double vdwxc_calculate_radial_hartree(int N, double dr, double* rho_i, double* dedn_i)
{
    double* rho_k = fftw_malloc(2 * sizeof(double) * N);
    double* kernel_i = fftw_malloc(2 * sizeof(double) * N);
    double* kernel_k = fftw_malloc(2 * sizeof(double) * N);
    int i;
    for (i=0; i<2*N; i++) {
        kernel_i[i] = 1.0;
    }
    vdwxc_fourier_bessel_transform(N, 2*N, 1, 1, dr, false, rho_i, rho_k, VDW_FBT_FORWARD);
    vdwxc_fourier_bessel_transform(2*N, 2*N, 1, 1, dr, true, kernel_i, kernel_k, VDW_FBT_FORWARD);
    for (i=0; i<2*N; i++) {
        rho_k[i] *= kernel_k[i];
    }

    vdwxc_fourier_bessel_transform(N, 2*N, 1, 1, dr, false, rho_i, rho_k, VDW_FBT_BACKWARD);
    fftw_free(rho_k);
    fftw_free(kernel_i);
    fftw_free(kernel_k);
    return 0.0;
}



double vdwxc_calculate_radial(vdwxc_data data, int N, double dr, double* rho_i,
                            double* sigma_i, double* dedn_i,
                            double* dedsigma_i)
{
    START_TIMER(radial, "vdw_df_calculate_radial");
    int pad = 2;
    assert(0);

    printf("N: %d\n", N);
    double* radial_q0_g = fftw_malloc(sizeof(double) * N);
    double* radial_dq0_drho_g = fftw_malloc(sizeof(double) * N);
    double* radial_dq0_dsigma_g = fftw_malloc(sizeof(double) * N);

    double* radial_theta_ga = fftw_malloc(sizeof(double) * SPLINE_ALPHAS * N);
    double* radial_F_ga     = fftw_malloc(sizeof(double) * SPLINE_ALPHAS * N);

    double* radial_theta_ka = fftw_malloc(pad * sizeof(double) * SPLINE_ALPHAS * N);
    double* radial_F_ka     = fftw_malloc(pad * sizeof(double) * SPLINE_ALPHAS * N);

    printf("Allocated buffers\n");

    vdwxc_write_data(N, dr, rho_i, "rho_input.dat");
    printf("Wrote data\n");

    START_TIMER(q0, "q0");
    printf("Started q0 timer\n");
    //printf("%d\n",data);
    //vdwxc_calculate_q0(N, data->Z_ab, data->q_cut,
    //                 rho_i, sigma_i, radial_q0_g,
    //                 radial_dq0_drho_g, radial_dq0_dsigma_g);
    END_TIMER(q0, "q0");

    START_TIMER(thetas, "thetas");
    //vdwxc_calculate_thetas(1, 1, N, 1,
    //                 rho_i, radial_q0_g,
    //                 radial_theta_ga);
    END_TIMER(thetas, "thetas");

    int a1;
    int a2;
    for (a1=0; a1<SPLINE_ALPHAS; a1++) {
        vdwxc_fourier_bessel_transform(N, pad*N,
                                     SPLINE_ALPHAS, SPLINE_ALPHAS,
                                     dr, false,
                                     radial_theta_ga+a1, radial_theta_ka+a1,
                                     VDW_FBT_FORWARD);

    }

    // Convolution
    double kernel_aa[SPLINE_ALPHAS*SPLINE_ALPHAS];
    double energy = 0.0;
    complex double F_a[SPLINE_ALPHAS];

    int k;
    int g;
    double L = dr * N;
    double dk = VDW_PI / (L * pad);

    for (k=0; k<pad*N; k++) {
        double kk = k * dk;
        double weight = 4*VDW_PI*kk*kk*dk;
        //vdwxc_interpolate_kernels(kk, kernel_aa);
        assert(0);
        double* ikernel_aa = kernel_aa;
        for (a1=0; a1 < SPLINE_ALPHAS; a1++) {
            double F = 0.0;
            for (a2=0; a2 < SPLINE_ALPHAS; a2++) {
                // This array should be completely contiguous and needs no fancy indexing.
                double a2val = radial_theta_ka[k * SPLINE_ALPHAS + a2];
                double kernel = *ikernel_aa++;
                double dF = a2val * kernel;
                F += dF;
            }
            //assert(kindex < data->icell.Nlocal[0]*data->icell.Nlocal[1]*data->icell.Nlocal[2]);
            double a1val = radial_theta_ka[k * SPLINE_ALPHAS + a1];
            energy += a1val * F * weight;
            F_a[a1] = F;
        }
        for(a1=0; a1 < SPLINE_ALPHAS; a1++) {
            radial_F_ka[k * SPLINE_ALPHAS + a1] = F_a[a1];
        }
    }

    // Todo: Figure out what is going on in the prefactor.
    energy *= 0.5 * (L)*(L)*dr*dr/(4*VDW_PI)/(4*VDW_PI)/pow(2*VDW_PI,3.0/2);
    /*for(a1=0; a1 < SPLINE_ALPHAS; a1++) {
       for (k=0; k<data->rgd.Nr*2; k++) {
         assert (radial_theta_ka[k * SPLINE_ALPHAS + a1] == 0);
       }
    }*/

    for (a1=0; a1<SPLINE_ALPHAS; a1++) {
        vdwxc_fourier_bessel_transform(N, pad*N,
                                     SPLINE_ALPHAS, SPLINE_ALPHAS,
                                     dr, false,
                                     radial_F_ga+a1, radial_F_ka+a1,
                                     VDW_FBT_BACKWARD);
    }

    /*
    for(a1=0; a1 < SPLINE_ALPHAS; a1++) {
       for (k=0; k<data->rgd.Nr; k++) {
         //printf("%d %d %.15f\n",a1,k, radial_theta_ga[k * SPLINE_ALPHAS + a1]);
         assert (radial_theta_ga[k * SPLINE_ALPHAS + a1] == 0);
       }
    }*/


    int alpha;

    double p_a[SPLINE_ALPHAS];
    double dpdq_a[SPLINE_ALPHAS];
    double prefactor = 0.5 / pad;
    for (g=0; g<N; g++) {
        double rho_dq0_drho = rho_i[g] * radial_dq0_drho_g[g];
        double rho_dq0_dsigma = rho_i[g] * radial_dq0_dsigma_g[g];
        dedn_i[g] = 0.0;
        dedsigma_i[g] = 0.0; // zero or not?
        //vdw_evaluate_palpha_splines(1.0, radial_q0_g[g], p_a);
        //vdw_evaluate_palpha_splines_derivative(radial_q0_g[g], dpdq_a);
        assert(0);
        for(alpha=0; alpha<SPLINE_ALPHAS; alpha++) {
            dedn_i[g] += prefactor * radial_F_ga[alpha+g*SPLINE_ALPHAS] *
                        (p_a[alpha] + dpdq_a[alpha] * rho_dq0_drho);
            dedsigma_i[g] += prefactor * radial_F_ga[alpha+g*SPLINE_ALPHAS] *
                        dpdq_a[alpha] * rho_dq0_dsigma;
        }
    }


    fftw_free(radial_q0_g);
    fftw_free(radial_dq0_drho_g);
    fftw_free(radial_dq0_dsigma_g);
    fftw_free(radial_theta_ga);
    fftw_free(radial_theta_ka);
    fftw_free(radial_F_ka);
    fftw_free(radial_F_ga);
    END_TIMER(radial, "vdw_df_calculate_radial");

    printf("vdw-energy %.10f \n", energy);
    return energy;

    // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    /* XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx
      data->radial_plan_r2r =
      fftw_plan_many_dft_r2r(1, // 1D FFT
      &data->rgd.Nr, // Table of 1 number, number of grid points
      SPLINE_ALPHAS, // how many FFTs
      radial_theta_ga, // Input array
      NULL, // full array (no subarray)
      SPLINE_ALPHAS, // Concecutive grid points are within 20 doubles
      1, // Individual transforms are next to eachother in memory
      radial_theta_ka, // Output array
      NULL, // full array (no subarray)
      SPLINE_ALPHAS,
      1,
      FFTW_RODFT10,
      FFTW_ESTIMATE);

    */


    //START_TIMER(potential, "potential");
    //END_TIMER(potential, "potential");


    // store integrals somehow for checking/debugging?
    return energy;
}


