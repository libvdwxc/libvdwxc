#include <mpi.h>
#include <assert.h>
#include <complex.h>
#include <pfft.h>

#include "vdwxc.h"
#include "vdwxc_mpi.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "vdw_core.h"


void vdwxc_set_communicator_pfft(vdwxc_data data, MPI_Comm mpi_comm,
                                 int nproc1, int nproc2)
{
    vdwxc_set_communicator(data, mpi_comm);
    assert(nproc1 * nproc2 == data->mpi_size);
    data->fft_type = VDW_PFFT;
    data->pfft_grid[0] = nproc1;
    data->pfft_grid[1] = nproc2;
    MPI_Comm mpi_cart_comm;
    pfft_create_procmesh_2d(mpi_comm, nproc1, nproc2, &mpi_cart_comm);
    data->mpi_cart_comm = mpi_cart_comm;
}

void vdwxc_init_pfft(vdwxc_data data, MPI_Comm mpi_comm, int nproc1, int nproc2)
{
    pfft_init(); // May be called any number of times
    vdwxc_set_communicator_pfft(data, mpi_comm, nproc1, nproc2);

    const ptrdiff_t ngpts[3] = {data->cell.Nglobal[0], data->cell.Nglobal[1], data->cell.Nglobal[2]};

    ptrdiff_t local_n[3], local_start[3];
    ptrdiff_t local_ni[3], local_i_start[3], local_no[3], local_o_start[3];

    ptrdiff_t r2c_alloc = pfft_local_size_many_dft_r2c(3,
                                                       ngpts,
                                                       ngpts, // ni (pruned) = ngpts for now
                                                       ngpts, // no (pruned) = ngpts for now
                                                       data->kernel.nalpha,
                                                       PFFT_DEFAULT_BLOCKS,
                                                       PFFT_DEFAULT_BLOCKS,
                                                       data->mpi_cart_comm,
                                                       PFFT_TRANSPOSED_OUT,
                                                       local_ni,
                                                       local_i_start,
                                                       local_n,
                                                       local_start);
    ptrdiff_t c2r_alloc = pfft_local_size_many_dft_c2r(3,
                                                       ngpts,
                                                       ngpts,
                                                       ngpts,
                                                       data->kernel.nalpha,
                                                       PFFT_DEFAULT_BLOCKS,
                                                       PFFT_DEFAULT_BLOCKS,
                                                       data->mpi_cart_comm,
                                                       PFFT_TRANSPOSED_IN,
                                                       local_n,
                                                       local_start,
                                                       local_no,
                                                       local_o_start);

    int i;
    for(i=0; i < 3; i++) {
        data->cell.Nlocal[i] = local_ni[i];
        data->cell.offset[i] = local_i_start[i];
        data->icell.Nlocal[i] = local_n[i];
        data->icell.offset[i] = local_start[i];
    }

    data->kLDA = data->icell.Nlocal[2];
    data->gLDA = data->cell.Nlocal[2];

    ptrdiff_t pfft_alloc = r2c_alloc > c2r_alloc ? r2c_alloc : c2r_alloc;
    data->work_ka = pfft_alloc_complex(pfft_alloc);

    data->pfft_plan_r2c = pfft_plan_many_dft_r2c(3,
                                                 ngpts,
                                                 ngpts,
                                                 ngpts,
                                                 data->kernel.nalpha,
                                                 PFFT_DEFAULT_BLOCKS,
                                                 PFFT_DEFAULT_BLOCKS,
                                                 (double*)data->work_ka,
                                                 data->work_ka,
                                                 data->mpi_cart_comm,
                                                 PFFT_FORWARD,
                                                 PFFT_ESTIMATE|PFFT_TRANSPOSED_OUT);
    data->pfft_plan_c2r = pfft_plan_many_dft_c2r(3,
                                                 ngpts,
                                                 ngpts,
                                                 ngpts,
                                                 data->kernel.nalpha,
                                                 PFFT_DEFAULT_BLOCKS,
                                                 PFFT_DEFAULT_BLOCKS,
                                                 data->work_ka,
                                                 (double*)data->work_ka,
                                                 data->mpi_cart_comm,
                                                 PFFT_BACKWARD,
                                                 PFFT_ESTIMATE|PFFT_TRANSPOSED_IN);
    assert(data->pfft_plan_r2c != NULL);
    assert(data->pfft_plan_c2r != NULL);
    vdwxc_allocate_buffers(data);
}
