#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "vdw_core.h"

#ifdef HAVE_MPI
#include <fftw3-mpi.h>
#include <mpi.h>
#include "vdwxc_mpi.h"
#else
#include "vdwxc.h"
#endif

#ifdef HAVE_PFFT
#include <pfft.h>
#endif


// Function for comparing 3d array shapes
int dimcmp(int* dim1, int* dim2) {
    int i;
    for(i = 0; i < 3; i++) {
        if(dim1[i] != dim2[i]) {
            return 0;
        }
    }
    return 1;
}

// Example how to use libvdwdf
int main(int argc, char** argv)
{
    vdwxc_data vdw = vdwxc_new(FUNC_VDWDF);
#ifdef HAVE_MPI
    MPI_Init(&argc, &argv);
    fftw_mpi_init();

    MPI_Comm comm = MPI_COMM_WORLD;
    int rank, size;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
#else
    int rank = 0;
    int size = 1;
#endif

    int dim[3] = {63, 72, 40};

    int i;
    int fft_type = -1;

    int nparsed_dims = 0;
    int parsed_dims[5] = {0, 0, 0, 0, 0};
    int nparsed_pfft_dims = 0;
    int pfft_grid[2] = {size, 1};

    i = 1;
    while(i < argc) {
        char* arg = argv[i++];
        if(strcmp(arg, "serial") == 0) {
            fft_type = VDW_FFTW_SERIAL;
        } else if (strcmp(arg, "mpi") == 0) {
            fft_type = VDW_FFTW_MPI;
        } else if (strcmp(arg, "pfft") == 0) {
            fft_type = VDW_PFFT;
        } else {
            int val = atoi(arg);
            assert(val != 0);
            if(fft_type == -1) {
                parsed_dims[nparsed_dims++] = val;
            } else {
                pfft_grid[nparsed_pfft_dims++] = val;
            }
        }
    }

    if(fft_type == -1) {
        fft_type = VDW_FFTW_MPI;
    }

    if(nparsed_dims == 1) {
        assert(parsed_dims[0] > 0);
        for(i = 0; i < 3; i++) {
            dim[i] = parsed_dims[0];
        }
    } else if(nparsed_dims == 3) {
        for(i = 0; i < 3; i++) {
            dim[i] = parsed_dims[i];
            assert(dim[i] != 0);
        }
    } else {
        assert(nparsed_dims == 0);
    }

    if(nparsed_pfft_dims == 2) {
        assert(pfft_grid[0] * pfft_grid[1] == size);
    } else {
        assert(nparsed_pfft_dims == 0);
    }

    printf("shape %dx%dx%d cpus %d fft_type=%d\n", dim[0], dim[1], dim[2], size, fft_type);

    vdwxc_set_unit_cell(vdw, dim[0], dim[1], dim[2],
                        8.1,0.0,0.0,
                        0.0,9.1,0.0,
                        0.0,0.0,10.1);
    long ngpts = dim[0] * dim[1] * dim[2];

    if(fft_type == VDW_FFTW_MPI) {
#ifdef HAVE_MPI
        vdwxc_init_mpi(vdw, comm);
#else
        printf("No MPI\n");
        assert(0);
#endif
    } else if(fft_type == VDW_PFFT) {
#ifdef HAVE_PFFT
        pfft_init();
        vdwxc_init_pfft(vdw, comm, pfft_grid[0], pfft_grid[1]);
#else
        printf("No PFFT\n");
        assert(0);
#endif
    } else {
        printf("init serial\n");
        vdwxc_init_serial(vdw);
    }

    int j, k;
    long ii;

    double* rho_i = (double*)malloc(ngpts * sizeof(double));
    double* sigma_i = (double*)malloc(ngpts * sizeof(double));
    double* v_i = (double*)malloc(ngpts * sizeof(double));
    double* dedsigma_i = (double*)malloc(ngpts * sizeof(double));

    for (ii=0; ii < ngpts; ii++) {
        rho_i[ii] = 1.0;
        sigma_i[ii] = 0.0;
        v_i[ii] = 0.0;
        dedsigma_i[ii] = 0.0;
    }

    struct vdwxc_unit_cell* cell = &vdw->cell;

    int ldim[3];
    for(i = 0; i < 3; i++) {
        ldim[i] = cell->Nlocal[i];
    }

    for(i=0; i < ldim[0]; i++) {
        for(j=0; j < ldim[1]; j++) {
            for(k=0; k < ldim[2]; k++) {
                int i0 = i + cell->offset[0];
                int j0 = j + cell->offset[1];
                int k0 = k + cell->offset[2];
                rho_i[k + ldim[2] * (j + ldim[1] * i)] = 2. + cos((i0 - j0 - k0) / 5.);
                sigma_i[k + ldim[2] * (j + ldim[1] * i)] = 2. + cos((j0 - k0) / 5.0);
            }
        }
    }

    vdwxc_print(vdw);
    double energy = vdwxc_calculate(vdw, rho_i, sigma_i, v_i, dedsigma_i);

    //printf("hmmmm v0 %e dedsigma0 %e\n", v_i[0], dedsigma_i[0]);
    double integrals[3] = {0.0, 0.0, 0.0};
    for(i=0; i < ngpts; i++) {
        integrals[0] += v_i[i] * rho_i[i];
        integrals[1] += dedsigma_i[i] * rho_i[i];
    }
    integrals[2] = energy;
    double dV = cell->dV;
    integrals[0] *= dV;
    integrals[1] *= dV;
#ifdef HAVE_MPI
    MPI_Allreduce(MPI_IN_PLACE, integrals, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif
    energy = integrals[2];


    if(rank == 0) {
        printf("Energy %20.15f :: nv-int %20.15f :: nsigma-int %20.15f\n",
               energy, integrals[0], integrals[1]);
    }

    // Standard test with "reasonable" but not too "nice" sizes
    int refdim1[3] = {63, 72, 40};
    // FFTW can run 1x1x1 transforms, but only howmany=1.  The smallest transform we can do is thus 1x1x2.
    int refdim2[3] = {1, 1, 2};
    int refdim3[3] = {4, 4, 4};

    int has_ref = 0;
    double Eref = -1e300;
    double nvref = -1e300;
    double nsigmaref = -1e300;
    if(dimcmp(dim, refdim1)) {
        has_ref = 1;
        Eref = 0.437482182604811;
        nvref = 0.262588806349290;
        nsigmaref = 0.002995413693451;
    } else if(dimcmp(dim, refdim2)) {
        // This is the smallest case we can run.
        // It is bearable to read the input parameters then.  So print them
        if(rank == 0) {
            printf("  rho = %24.18f %24.18f\n", rho_i[0], rho_i[1]);
            printf("sigma = %24.18f %24.18f\n", sigma_i[0], sigma_i[1]);
        }
        has_ref = 1;
        Eref = -0.870737655962683;
        nvref = -1.624693317322961;
        nsigmaref = 0.000136231643227;
    } else if(dimcmp(dim, refdim3)) {
        has_ref = 1;
        Eref = -0.810380968889586;
        nvref = -1.505252254148879;
        nsigmaref = 0.000163549311654;
    }

    if(has_ref) {
        double eerr = fabs(Eref - energy);
        double nverr = fabs(nvref - integrals[0]);
        double nsigmaerr = fabs(nsigmaref - integrals[1]);

        if(rank == 0) {
            printf("errs   %20.15e ::        %20.15e ::            %20.15e\n",
                   eerr, nverr, nsigmaerr);
        }
        assert(eerr < 5e-14);
        assert(nverr < 5e-14);
        assert(nsigmaerr < 1e-14);
    } else {
        if(rank == 0) {
            printf("(No ref)\n");
        }
    }

    vdwxc_finalize(&vdw);
#ifdef HAVE_MPI
    MPI_Finalize();
#endif
    assert(vdw == NULL);

    free(v_i);
    free(dedsigma_i);
    free(sigma_i);
    free(rho_i);
    return 0;
}
