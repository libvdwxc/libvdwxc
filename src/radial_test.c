#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <fftw3.h>

#include "vdwxc.h"
#include "vdw_radial.h"
#include "vdw_core.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif


void derivative(int N, double dr, double* in, double* out)
{
    out[0] =(in[1]-in[0])/dr;
    int i;
    for (i=1; i<N-1; i++) {
        out[i] = 0.5*(in[i+1]-in[i-1])/dr;
    }
    out[N-1] = (in[N-1]-in[N-2])/dr;
}

void write_data(int N, double dr, double* data, char* file)
{
    FILE *fp;
    fp = fopen(file, "w");
    int g;
    for (g=0; g<N; g++) {
        fprintf(fp, "%.10f %.10f\n", g*dr, data[g]);
    }
    fclose(fp);
}

double potential(int N, double dr, double* rho_i, double* sigma_i, double* dedn_i, double* dedsigma_i)
{
    vdwxc_data data = vdwxc_new(FUNC_VDWDF);

    double E = vdwxc_calculate_radial(data, N, dr, rho_i, sigma_i,
                                      dedn_i, dedsigma_i);

    printf("Energy %.15f\n", E);
    vdwxc_finalize(&data);
    return E;
}

void read_data(int N, double* data, char* filename)
{
    FILE *fp;
    fp = fopen (filename, "rt");
    int i;
    for (i=0; i<N; i++) {
       char line[80];
       if (!fgets(line, 80, fp)) {
           break;
       }
       sscanf (line, "%lf", &data[i]);
    }
    fclose(fp);
}

void hartree_test()
{
    double dr = 0.1;
    int N = 100;

    vdwxc_data data = vdwxc_new(FUNC_VDWDF);

    double* rho_i = fftw_malloc(sizeof(double) * N);
    double* v_i = fftw_malloc(sizeof(double) * N);
    double* ref_v_i = fftw_malloc(sizeof(double) * N);

    // Set up gaussian density
    double sigma = 1.0;
    int i;
    for (i=0; i<N; i++) {
        double r = i * dr;
        rho_i[i] = 1.0 / (sigma*sigma*sigma) / pow(2*VDW_PI, 3.0/2.0) * exp(-r*r/(2*sigma*sigma));
        ref_v_i[i] = erf( r / (sqrt(2) * sigma)) / r;
    }
    write_data(N, dr, ref_v_i, "ref_v_i.txt");

    vdwxc_calculate_radial_hartree(N, dr, rho_i, v_i);

    vdwxc_finalize(&data);
    free(rho_i);
    free(v_i);
    free(ref_v_i);
}

int main(int argc, char** argv)
{
    // Set up grid parameters
    int N = 136*2;
    double dr = 16.0 / (136.0*2) / 0.529177;

    double* rho_i = fftw_malloc(sizeof(double) * N);
    double* sigma_i = fftw_malloc(sizeof(double) * N);
    double* dedn_i = fftw_malloc(sizeof(double) * N);
    double* dedsigma_i = fftw_malloc(sizeof(double) * N);
    double* ref_dedn_i = fftw_malloc(sizeof(double) * N);
    double* ref_dedsigma_i = fftw_malloc(sizeof(double) * N);

    // Read density from a file
    read_data(N, rho_i, "testdata/rho.txt");
    read_data(N, sigma_i, "testdata/sigma.txt");
    read_data(N, ref_dedn_i, "testdata/potential.txt");
    read_data(N, ref_dedsigma_i, "testdata/dedsigma.txt");

    int i;
    for (i=N/2; i<N; i++) rho_i[i] = 0;
    for (i=N/2; i<N; i++) sigma_i[i] = 0;
    //potential(N, dr, rho_i, dedn_i);
    printf("Evaluting potential.\n");
    double E1 = potential(N, dr, rho_i, sigma_i, dedn_i, dedsigma_i);
    printf("Total energy %.15f\n", E1);
    /*
    // Manually get the potential with finite difference
    // This does not work, because we are varying shells of density due to spherical symmetry,
    // and vdW-DF is non-local.
    double eps = 1e-6;
    for (i=0; i<N/2; i++)
    {
       rho_i[i] += eps;
       double E2 = potential(N, dr, rho_i, sigma_i, temp_dedn_i);
       rho_i[i] -= eps;
       dedn_i[i] = (E2-E1) / eps;
       printf("%.10f %.10f\n", i*dr, dedn_i[i]);
    }
    */

    // plot "refdedn.dat" u 1:2, "dedn.dat" u 1:2, "refdedsigma.dat" u 1:2, "dedsigma.dat" u 1:2
    write_data(N, dr, dedn_i, "dedn.dat");
    write_data(N, dr, ref_dedn_i, "refdedn.dat");
    write_data(N, dr, dedsigma_i, "dedsigma.dat");
    write_data(N, dr, ref_dedsigma_i, "refdedsigma.dat");

    return 0;
}
