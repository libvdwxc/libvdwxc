#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "vdwxc.h"
//#include "vdw_q0.h"

double calculate(vdwxc_data vdw, double *rho_g, double *sigma_g,
                 double *dedrho_g, double *dedsigma_g, int npts, int nspin)
{
    double energy;
    if (nspin == 1) {
        energy = vdwxc_calculate(vdw, rho_g, sigma_g, dedrho_g, dedsigma_g);
    } else {
        energy = vdwxc_calculate_spin(vdw,
                                      rho_g, rho_g + npts,
                                      sigma_g, sigma_g + npts,
                                      dedrho_g, dedrho_g + npts,
                                      dedsigma_g, dedsigma_g + npts);
    }
    return energy;
}


void test(double rhomin, double rhomax, int nspin)
{
    int nx = 2;
    int ny = 2;
    int nz = 4;

    int npts = nx * ny * nz;

    double rho_g[npts * nspin];
    double sigma_g[npts * nspin];

    double dedrho_g[npts * nspin];
    double dedsigma_g[npts * nspin];

    double spacing = 0.5;
    double dv = spacing * spacing * spacing;

    int i;
    for(i = 0; i < npts; i++) {
        double fraction = (1.0 * i) / (npts - 1);
        double rho = exp(log(rhomin) + fraction * (log(rhomax) - log(rhomin)));
        double factor = 2.0 * sin(1e7 * i);
        double sigma = rho * factor * factor;
        sigma *= sigma;
        rho_g[i] = rho;
        sigma_g[i] = sigma;
        dedrho_g[i] = 0.0;
        //printf("rho %e\n", rho);
        dedsigma_g[i] = 0.0;
        //printf("%e %e\n", rho, sigma);
    }
    for(i = npts; i < nspin * npts; i++) {
        double x = 2.0 * sin(1e6 * i);
        x *= x;
        rho_g[i] = x * rho_g[i - npts];
        sigma_g[i] = x * sigma_g[i - npts];
        dedrho_g[i] = 0.0;
        dedsigma_g[i] = 0.0;
    }

    vdwxc_data vdw;
    if(nspin == 1) {
        vdw = vdwxc_new(VDWXC_DF1);
    } else {
        vdw = vdwxc_new_spin(VDWXC_DF1);
    }
    vdwxc_set_unit_cell(vdw, nx, ny, nz,
                        spacing * nx, 0.0, 0.0,
                        0.0, spacing * ny, 0.0,
                        0.0, 0.0, spacing * nz);
    vdwxc_init_serial(vdw);
    vdwxc_print(vdw);

    double energy = calculate(vdw, rho_g, sigma_g, dedrho_g, dedsigma_g, npts, nspin);


    double q0_g[npts];
    vdwxc_get_q0(vdw, q0_g);

    printf("energy %.16e Ha\n", energy);

    double dedrho_unused[npts * nspin];
    double dedsigma_unused[npts * nspin];

    for(i=0; i < npts * nspin; i++) {
        double tmprho = rho_g[i];
        double tmpsigma = sigma_g[i];
        double dedrho_fd;

        int j;
        for(j=0; j < npts * nspin; j++) {
            dedrho_unused[j] = 0.0;
            dedsigma_unused[j] = 0.0;
        }

        double drho = rho_g[i] * 1e-4;
        double dsigma = sigma_g[i] * 1e-4;

        rho_g[i] -= 2.0 * drho;
        double e_m2 = calculate(vdw, rho_g, sigma_g, dedrho_unused, dedsigma_unused, npts, nspin);
        rho_g[i] += drho;
        double e_m1 = calculate(vdw, rho_g, sigma_g, dedrho_unused, dedsigma_unused, npts, nspin);
        rho_g[i] += 2.0 * drho;
        double e_p1 = calculate(vdw, rho_g, sigma_g, dedrho_unused, dedsigma_unused, npts, nspin);
        rho_g[i] += drho;
        double e_p2 = calculate(vdw, rho_g, sigma_g, dedrho_unused, dedsigma_unused, npts, nspin);
        dedrho_fd = (e_m2 + 8.0 * (-e_m1 + e_p1) - e_p2) / (12.0 * drho * dv);

        rho_g[i] = tmprho;

        sigma_g[i] -= 2.0 * dsigma;
        double es_m2 = calculate(vdw, rho_g, sigma_g, dedrho_unused, dedsigma_unused, npts, nspin);
        sigma_g[i] += dsigma;
        double es_m1 = calculate(vdw, rho_g, sigma_g, dedrho_unused, dedsigma_unused, npts, nspin);
        sigma_g[i] += 2.0 * dsigma;
        double es_p1 = calculate(vdw, rho_g, sigma_g, dedrho_unused, dedsigma_unused, npts, nspin);
        sigma_g[i] += dsigma;
        double es_p2 = calculate(vdw, rho_g, sigma_g, dedrho_unused, dedsigma_unused, npts, nspin);

        double dedsigma_fd;
        if(tmpsigma != 0) {
            dedsigma_fd = (es_m2 + 8.0 * (-es_m1 + es_p1) - es_p2) / (12.0 * dsigma * dv);
        } else {
            dedsigma_fd = 0.0;  // Not applicable
        }

        sigma_g[i] = tmpsigma;

        double err = fabs(dedrho_fd - dedrho_g[i]);
        double rel_err = 1.0 - dedrho_g[i] / dedrho_fd;
        double sigma_err = fabs(dedsigma_fd - dedsigma_g[i]);
        double rel_sigma_err = 1.0 - dedsigma_g[i] / dedsigma_fd;
        printf("\n");
        printf("n=%.2e sigma=%.2e q0=%.2e\n",
               rho_g[i], sigma_g[i], q0_g[i % npts]);
        printf("dedn=%.3e dedn_fd=%.3e err=%.3e rel=%.3e\n",
               dedrho_g[i], dedrho_fd, err, rel_err);
        if (tmpsigma != 0) {
            printf("dedsigma=%.3e dedsigma_fd=%.3e err=%.3e rel=%.3e\n",
                   dedsigma_g[i], dedsigma_fd, sigma_err, rel_sigma_err);
        } else {
            printf("(sigma = 0, no derivative)\n");
        }
    }
    vdwxc_finalize(&vdw);
}

int main(int argc, char **argv)
{
    test(1e-16, 1e-10, 1);
    test(1e-10, 1e-4, 1);
    test(1e-4, 1e1, 1);
    test(1e-4, 1e1, 2);
    return 0;
}
