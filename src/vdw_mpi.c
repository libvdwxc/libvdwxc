#include <complex.h>
#include <assert.h>
#include <mpi.h>
#include <fftw3-mpi.h>

#include "vdwxc.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "vdw_core.h"


void vdwxc_set_communicator(vdwxc_data data, MPI_Comm mpi_comm)
{
    assert(data->mpi_comm == NULL);
    data->fft_type = VDW_FFTW_MPI;
    MPI_Comm_rank(mpi_comm, &data->mpi_rank);
    MPI_Comm_size(mpi_comm, &data->mpi_size);
    data->mpi_comm = mpi_comm;
}

void vdwxc_init_mpi(vdwxc_data data, MPI_Comm mpi_comm)
{
    fftw_mpi_init(); // May be called any number of times
    vdwxc_set_communicator(data, mpi_comm);

    const ptrdiff_t plan_dims[3] = {data->cell.Nglobal[0], data->cell.Nglobal[1], data->cell.Nglobal[2]};
    // Here we need to allocate a theta_ak and theta_ag for use with FFTW.
    ptrdiff_t local_size_dims[3];
    ptrdiff_t fftw_alloc_size;
    data->kLDA = data->icell.Nglobal[2];
    data->gLDA = 2 * data->kLDA;
    local_size_dims[0] = data->cell.Nglobal[0];
    local_size_dims[1] = data->cell.Nglobal[1];
    local_size_dims[2] = data->kLDA;

    ptrdiff_t fftw_xsize, fftw_xstart, fftw_ysize, fftw_ystart;

    fftw_alloc_size = fftw_mpi_local_size_many_transposed(3,
                                                          local_size_dims,
                                                          data->kernel.nalpha,
                                                          FFTW_MPI_DEFAULT_BLOCK,
                                                          FFTW_MPI_DEFAULT_BLOCK,
                                                          data->mpi_comm,
                                                          &fftw_xsize,
                                                          &fftw_xstart,
                                                          &fftw_ysize,
                                                          &fftw_ystart);
    data->cell.Nlocal[0] = fftw_xsize;
    data->cell.Nlocal[1] = data->cell.Nglobal[1];
    data->cell.Nlocal[2] = data->cell.Nglobal[2];
    data->cell.offset[0] = fftw_xstart;

    data->icell.Nlocal[0] = data->cell.Nglobal[0];
    data->icell.Nlocal[1] = fftw_ysize;
    data->icell.Nlocal[2] = data->icell.Nglobal[2];
    data->icell.offset[1] = fftw_ystart;

    assert(fftw_alloc_size % data->kernel.nalpha == 0);
    data->work_ka = fftw_alloc_complex(fftw_alloc_size);

    // TODO custom strategy FFT_ESTIMATE/MEASURE/etc.
    data->plan_r2c = fftw_mpi_plan_many_dft_r2c(3,
                                                plan_dims,
                                                data->kernel.nalpha,
                                                FFTW_MPI_DEFAULT_BLOCK,
                                                FFTW_MPI_DEFAULT_BLOCK,
                                                (double*)data->work_ka,
                                                data->work_ka,
                                                data->mpi_comm,
                                                FFTW_ESTIMATE|FFTW_MPI_TRANSPOSED_OUT);
    data->plan_c2r = fftw_mpi_plan_many_dft_c2r(3,
                                                plan_dims,
                                                data->kernel.nalpha,
                                                FFTW_MPI_DEFAULT_BLOCK,
                                                FFTW_MPI_DEFAULT_BLOCK,
                                                data->work_ka,
                                                (double*)data->work_ka,
                                                data->mpi_comm,
                                                FFTW_ESTIMATE|FFTW_MPI_TRANSPOSED_IN);
    assert(data->plan_r2c != NULL);
    assert(data->plan_c2r != NULL);
    vdwxc_allocate_buffers(data);
}

// We want vdwxc_init_pfft to be defined even when we do not have PFFT.
// That way, codes do not need to manage precompiler arguments to check
// for which symbols exist in libvdwxc, except for MPI where each DFT
// code probably has a precompiler variable anyway.
#ifndef HAVE_PFFT
void vdwxc_init_pfft(vdwxc_data data, MPI_Comm comm, int proc1, int proc2)
{
    assert(0);
}
#endif
